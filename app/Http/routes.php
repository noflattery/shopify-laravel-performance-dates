<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::group(['middleware' => ['web']], function(){
	Route::get('/', 'Auth\ShopifyAuthController@access');
	Route::get('authCallback', 'Auth\ShopifyAuthController@authCallback');
	$shopify = App::make('ShopifyAPI', [
	'API_KEY' => '7b205238679f9e4a72f4abd22ac10135',
	'API_SECRET' => '6d7dfc2595fa308e22ac0a36f267c464',
	'SHOP_DOMAIN' => 'stage-p-dev.myshopify.com',
	'ACCESS_TOKEN' => '1277643e961771ecce4fcc5713ba7bd6'
	]);

	// Gets a list of products
	$result = $shopify->call([
	'METHOD'     => 'GET',
	'URL'         => '/admin/orders.json?page=1'
	]);

	$orders = $result->orders;
	// Print out the title of each product we received

    var_dump($orders);
	return view('orders')->with('order', $orders);

	
});

