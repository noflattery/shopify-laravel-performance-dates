<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Session;

class ShopifyAuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Shopify Auth Controller 
    |--------------------------------------------------------------------------
    |
    | The ShopifyAuthController works like this:

        Check is there ‘shop’ in incoming URL query, save it in session for future use if it exists.
        Check is there ‘access_token’ stored in session:
        
        if YES
        As long as we have access token, we can use it for Shopify API call
        
        If NO

        Run the Shopify API library to create a ShopifyAPI instance by passing App key / App Shared Secret / the current shop domain (from the URL query)
        Using the created ShopifyAPI instance to call for the installation URL
        Show a view (lets call this escape iframe view) that only has a JS code for a parent window redirection to the installation URL
        The YES case above is easy to understanding, but for the NO case this cost me quite a lot time to get it works. As your app is running inside Shopify interface using <iframe>, for the OAuth process, the way ask Shopify to provide you access of a Shopify store, it requires calling the parent window (the page holding the iframe, ie the Shopify admin page) for such authentication call.
    */

    /**
     *
     * @return void
     */
    public function access(Request $request)
    {
        $shop = '';
        $orders = '';

        if( isset ($_GET['shop']) ) {
            session(['shop' => $_GET['shop']]);
            $shop = $_GET['shop'];
        }
        if(Session::has('access_token'))
        {
            return view('welcome');
        } else {
            auth($shop);
            return view('orders')->with('orders', $orders);
        }
    }

    public function auth($shop)
    {
        $app_settings = DB::table('app_setting')->first();
        $shopify = App::make('ShopifyAPI', [
            'API_KEY' => $app_settings->api_key,
            'API_SECRET' => $app_settings->shared_secret,
            'SHOP_DOMAIN' => $shop,
            'ACCESS_TOKEN' => ''
        ]);

        $permission_url = $shopify->installURL(
        [
            'permissions' => array('read_orders', 'write_orders', 'read_products', 'write_products'),
            'redirect' => $app_settings->redirect_url
        ]);

        return view('auth.escapeIFrame', [ 'installUrl' => $permission_url ]);
    }
}
